// - Select the section with an id of container without using querySelector.

let sectionElement = document.getElementById('container') // getElementById takes id of an element as string and returns the first matched HTML element

// - Select the section with an id of container using querySelector.

let sectionElementWithQuerySelector = document.querySelector('#container') // querySelector takes valid css selector as string and returns the first matched HTML element

// - Select all of the list items with a class of "second".

let secondListItems = document.querySelectorAll('li.second') // QuerySelectorall takes valid css selector as string and returns array like nodeList Object

// - Select a list item with a class of third, but only the list item inside of the ol tag.

let olItemThird = document.querySelector('ol li.third')

// - Give the section with an id of container the text "Hello!".

sectionElement.append('Hello!') 

// - Add the class main to the div with a class of footer.

let footerDivEl = document.querySelector('div.footer')

footerDivEl.classList.add('main')

// - Remove the class main on the div with a class of footer.

footerDivEl.classList.remove('main')

// - Create a new li element.

let newLiElement = document.createElement('li')

// - Give the li the text "four".

newLiElement.classList.add('four')
newLiElement.textContent= 'four'

// - Append the li to the ul element.

let ulElement = document.querySelector('ul')
ulElement.appendChild(newLiElement)

// - Loop over all of the lis inside the ol tag and give them a background color of "green".

let olList = document.querySelectorAll('ol li')

for (let olListItem of olList) {
    olListItem.style.backgroundColor = "green"
}

// - Remove the div with a class of footer. 

footerDivEl.remove()

// End  
